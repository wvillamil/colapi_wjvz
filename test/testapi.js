var mocha = require('mocha');
var chai = require('chai');
var chaihttp = require('chai-http');
var serve = require('../server_v3');

var should =chai.should();

chai.use(chaihttp);

describe('pruebas BBVA', () => {
  it('Test Conectividad', (done) => {
    chai.request('http://www.bbva.com')
      .get('/')
      .end((err, res)=>{
          res.should.have.status(200);
          //console.log(res);
          done();
      })
  });
  it('Mi Colapi', (done) => {
    chai.request('http://localhost:3000')
      .get('/colapi/v3/users')
      .end((err, res)=>{
          res.should.have.status(200);
          done();
      })

  });
  it('Devuelve un array', (done) => {
    chai.request('http://localhost:3000')
      .get('/colapi/v3/users')
      .end((err, res)=>{
          res.body.should.be.a('array');
          done();
      })

  });
  it('Devuelve un elemento', (done) => {
    chai.request('http://localhost:3000')
      .get('/colapi/v3/users')
      .end((err, res)=>{
          res.body.length.should.be.gte(1);
          done();
      })
  });


    it('Tiene las propiedades Nombre y Email', (done) => {
        chai.request('http://localhost:3000')
          .get('/colapi/v3/users')
          .end((err, res)=>{
              console.log(res.body[0]);
              res.body[0].should.have.property('first_name');
              res.body[0].should.have.property('email');
              res.body[0].should.have.property('id');


              done();
          })

      });
      it('Valida login', (done) => {
          chai.request('http://localhost:3000')
            .post('/colapi/v3/login')
            .send('{"email":"gmatussov0@pcworld.com","password":"UXyVZpSDf"}')
            .end((err, res, body)=>{
                console.log(res.body);
                res.body.msd.should.be.eql("error datos ingresados")
                done();
            })

        });

});
