var express = require('express');
var bodyParser = require('body-parser');
var app = express();
var port = process.env.port || 3000;
var usersFile = require('./users.json');
var urlBase = "/colapi/v2/";

app.use(bodyParser.json());
//Servidor escuchando por el puerto 3000
app.listen(port);
console.log("Colapi Escuchando puerto :" + port +"...");
//peticion Log in
app.post(urlBase + "login",
 function (req, res) {
   console.log("Peticion Login: "+ urlBase + "login");
   console.log(req.body.email);
   console.log(req.body.password);
   var user = req.body.email;
   var pass = req.body.password;
   for(us of usersFile){
     if(us.email==user){
       if(us.password==pass){
         us.logged = true;
         writeUserDataToFile(usersFile);
         console.log("Login Correcto");
         res.send({"MSG" : "Login Correcto.", "idUsuario": us.id} );
       }else{
          res.send({"MSG" : "Login Incorrecto."} );
       }
     }
   }
   res.send({"MSG" : "usuario no definido."} );
});
//LogOut Colapi
app.post(urlBase + "logout",
  function (req, res) {
  console.log("Peticion logout: "+ urlBase + "logout");
  console.log(req.body.email);
  console.log(req.body.password);
  var user = req.body.email;
  var pass = req.body.password;
  for(us of usersFile){
    if(us.email==user){
      if(us.password==pass){
        delete us.logged;
        writeUserDataToFile(usersFile);
        console.log("Login Correcto");
        res.send({"MSG" : "logout Correcto.", "idUsuario": us.id} );
      }else{
         res.send({"MSG" : "Logout Incorrecto."} );
      }
    }
  }
  res.send({"MSG" : "usuario no definido."} );
});
//Funcion persiste en archivo
function writeUserDataToFile(data){
  var fs = require('fs');
  var jsonUserData = JSON.stringify(data);
  fs.writeFile("./users.json", jsonUserData, "utf8",
  function(err) {
    if(err){
      console.log(err);
    }else{
      console.log("Datos escritos en 'user.json'.");
    }
  })
}
//peticion Get con Parametros
app.get(urlBase + 'users/:id',
 function (req, res) {
   var pos = req.params.id;

   res.send(
    usersFile[pos-1]
    );
  console.log(urlBase + "users/id= " + pos);
});
//peticion Get con Query
app.get(urlBase + 'users',
 function (peticion, respuesta) {
   console.log(urlBase + "users/:id Peticion con Query");
   console.log(peticion.query);
   respuesta.send(
    {"msg" : "Get Con Query string"}
    );

});
//Metodo para crear un objeto user.
app.post(urlBase + "users",
 function (req, res) {
   console.log("Peticion Post: "+ urlBase + "users");
   console.log(req.body);
   //res.sendFile("users.json",{root:__dirname});
   var newID = usersFile.length + 1 ;
   var jsonID = {};
   jsonID=req.body;
   jsonID.id = newID;
   usersFile.push(jsonID);
   res.send({"MSG" : "Usuario nuevo  creado ok.", jsonID} );
});
//Metodo Put para modificar el recurso user
app.put(urlBase + 'users/:id',
 function (req, res) {
   console.log("PUT /colapi/v1/users");
   var idBuscar = req.params.id;
   var updateUser = req.body;
   var existe =0;
   console.log(updateUser);
   for (var user in usersFile){
     if(usersFile[user].id==idBuscar){
       console.log("encontrado " + user);
       existe =1;
     }
   }
   if (existe == 0){
     console.log("usuario con id " + idBuscar + "no existe");
   }
   res.send({
    "message" : "Put a Colapi en formato Json" ,
    "version" : "V1"
    });

});
//Metodo para eliminar objetos user
app.delete(urlBase + 'users/:id',
 function (req, res) {
   console.log("DELETE" + urlBase +" /colapi/v1");
   var idEliminar = req.params.id;
   for (var user in usersFile){
     if(usersFile[user].id==idEliminar){
       usersFile.splice(user,1);
       res.send({
        "message" : "Usuario eliminado " ,
        "id" : idEliminar
        });
     }
   }
   console.log("prueba res for");
   res.send({
    "message" : "Usuario a eliminar no Existe"
    });
});
