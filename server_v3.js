var express = require('express');
var bodyParser = require('body-parser');
var requestJson = require('request-json');
var app = express();
var port = process.env.port || 3000;
var usersFile = require('./users.json');

var urlBase = "/colapi/v3/";
var urlBaMongo = "https://api.mlab.com/api/1/databases/colapidb_wjvz/collections/";
var apiKeyMLab  ="apiKey=38GfdWDTkAR41Ny_W8hcbqtoRK4nnVk9";


app.use(bodyParser.json());
//Servidor escuchando por el puerto 3000
app.listen(port);
console.log("Colapi Escuchando puerto :" + port +"...");
//Log in Colapi
app.post(urlBase + "login",
 function (req, res) {
   console.log("Peticion Login: "+ urlBase + "login");
   var user = req.body.email;
   var pass = req.body.password;
   //llamado servicio MLAB
   var httpClient = requestJson.createClient(urlBaMongo);
   var querString = "q={'email':'" + user + "'}&f={'_id':0}&";
   httpClient.get('user?'+ querString + apiKeyMLab,function(err, respuestaMLab, body){
        var respget = body[0];
        if (respget!=undefined && respget.password==pass){
          respget.logged="true";
          //var cambio = '{"$set":' + JSON.stringify(respget) + '}'
          var cambio = '{"$set":{"logged":true}}'
          httpClient.put('user?q={"id": ' + respget.id + '}&' + apiKeyMLab, JSON.parse(cambio),
          function(err, respuestaMLab, bodypu){
            bodypu.n >0 ? res.send({"msg":"Usuario logueado exitosamente","id":respget.id}):res.send({"msg":"Error al realizar login intente de nuevo"});
          });
        }else {
          res.status(409);
          res.send({"msg":"error datos ingresados"});
        }
      });
});
//LogOut Colapi
app.post(urlBase + "logout",
  function (req, res) {
  console.log("Peticion logout: "+ urlBase + "logout");
  console.log(req.body.id);
  var user = req.body.id;
  var httpClient = requestJson.createClient(urlBaMongo);
  var querString = "q={'id':" + user + "}&f={'_id':0}&";
  httpClient.get('user?'+ querString + apiKeyMLab,
     function(err, respuestaMLab, body){
       var respget = body[0];
       if (respget!=undefined){
          delete respget.logged;
         var cambio = '{"$set":' + JSON.stringify(respget) + '}'
         httpClient.put('user?q={"id": ' + respget.id + '}&' + apiKeyMLab, respget,
         function(err, respuestaMLab, bodypu){
           bodypu.n >0 ? res.send({"msg":"Usuario deslogueado exitosamente"}):res.send({"msg":"Error al cerrra la sesion"});
         });
       }else{
         res.status(409);
         res.send({"msg":"error datos ingresados"});
       }
     });

});
//Funcion persiste en archivo
function writeUserDataToFile(data){
  var fs = require('fs');
  var jsonUserData = JSON.stringify(data);
  fs.writeFile("./users.json", jsonUserData, "utf8",
  function(err) {
    if(err){
      console.log(err);
    }else{
      console.log("Datos escritos en 'user.json'.");
    }
  })
}
//peticion Get con Parametros
app.get(urlBase + 'users/:id',
 function (req, res) {
   var idUsu = req.params.id;
   console.log("Get Users " + urlBase + "users/id= " + idUsu);
   var httpClient = requestJson.createClient(urlBaMongo);
   var querString = "q={'id':" + idUsu + "}&f={'_id':0}&";
   httpClient.get('user?'+ querString + apiKeyMLab, function(err, respuestaMLab, body){
     var respget=body[0];
     if(respget!=undefined){
       res.send(respget);
     }else{
       res.status(409);
       res.send({"msg":"Error al recuperar users de Mlab"})
     }
      });

});
//peticion cuentas  Get con Parametros
app.get(urlBase + 'users/:idUsu/accounts/:iban',
 function (req, res) {
   var idUsu = req.params.idUsu;
   var iban = req.params.iban;
   console.log("Consulta la cuenta puntual de un usuario");
   var httpClient = requestJson.createClient(urlBaMongo);
   var querString = "q={'IBAN':" + iban + "}&f={'_id':0}&";
   httpClient.get('account?'+ querString + apiKeyMLab,
    function(err, respuestaMLab, body){
      var respget=body[0];
      if(respget!=undefined){
        res.send(body);
      }else{
        res.status(409);
        res.send({"msg":"Error al recuperar users de Mlab"})
      }
      });
});

//peticion accounts Get sin parametros a MLAB
app.get(urlBase + 'users/:idUsu/accounts',
 function (peticion, res) {
   console.log(urlBase + "lista de cuenta por usuario");
   var idUsu = peticion.params.idUsu;
   var httpClient = requestJson.createClient(urlBaMongo);
   var querString = "q={'user_id':" + idUsu + "}&f={'_id':0}&";
   httpClient.get('account?'+ querString + apiKeyMLab, function(err, respuestaMLab, body){
     var respget=body[0];
     if(respget!=undefined){
       res.send(body);
     }else{
       res.status(409);
       res.send({"msg":"Error al recuperar users de Mlab"})
     }
      });
});

//peticion lists movimientos  por cuenta
app.get(urlBase + 'users/:idUsu/accounts/:iban/movements',
 function (req, res) {
   var idUsu = req.params.idUsu;
   var iban = req.params.iban;
   console.log("Lista los movimietnos de una cuenta");
   var httpClient = requestJson.createClient(urlBaMongo);
   var querString = "q={'IBAN':" + iban + "}&f={'_id':0}&";
   httpClient.get('movement?'+ querString + apiKeyMLab,
    function(err, respuestaMLab, body){
      var respget=body[0];
      console.log(respuestaMLab.statusCode);
      if(respget!=undefined){
            var decimals = 2;
            var separators = separators || ['.', "'", ','];
            for(var i=0;i<body.length;i++){
              var value=body[i].importe;
              var number = (parseFloat(value) || 0).toFixed(decimals);
              var parts = number.split(/[-.]/);
              value = parts[parts.length > 1 ? parts.length - 2 : 0];
              var result = value.substr(value.length - 3, 3) + (parts.length > 1 ?
                  separators[separators.length - 1] + parts[parts.length - 1] : '');
              var start = value.length - 6;
              var idx = 0;
              while (start > -3) {
                  result = (start > 0 ? value.substr(start, 3) : value.substr(0, 3 + start))
                      + separators[idx] + result;
                  idx = (++idx) % 2;
                  start -= 3;
              }
              body[i].importe=result;
            }
            res.send(body);
      }else{
        res.status(409);

        res.send({"msg":"No Existen movimientos a movimientos a mostrar"})
      }
      });
});

//peticion users Get a MLAB
app.get(urlBase + 'users',
 function (peticion, res) {
   console.log(urlBase + "users Peticion con Query");
   var httpClient = requestJson.createClient(urlBaMongo);
   var querString = "f={'_id':0}&";
   httpClient.get('user?'+ querString + apiKeyMLab, function(err, respuestaMLab, body){
     var respget=body[0];
     if(respget!=undefined){
       res.send(respget);
     }else{
       res.status(409);
       res.send({"msg":"Error al recuperar users de Mlab"})
     }
      });

});
//Metodo para crear un objeto user.
app.post(urlBase + "users",
 function (req, res) {
   console.log("usuarios Peticion Post: "+ urlBase + "users");
   var querString = "f={'_id':0,'id':1}&s={'id':-1}&l=1&";
   var httpClient = requestJson.createClient(urlBaMongo);
   httpClient.get('user?'+querString + apiKeyMLab, function(err, respuestaMLab, body){
        var maximo = body[0].id+1;
        req.body.id=maximo
        console.log(req.body);
        httpClient.post('user?'+apiKeyMLab,req.body,
         function(err, respuestaMLab, bodyp){
             var respget = !err? (body[0]==undefined ?204:body) :{"msg":"Error al recuperar users de Mlab"};
             res.send(bodyp);
             console.log(err);
           });
      });


});
//Metodo Put para modificar el recurso user
app.put(urlBase + 'users/:id',
 function (req, res) {
   console.log("PUT /colapi/v1/users");
   var idBuscar = req.params.id;
   var updateUser = req.body;
   var existe =0;
   var cambio = '{"$set":' + JSON.stringify(req.body) + '}'
   var httpClient = requestJson.createClient(urlBaMongo);
   httpClient.put('user?q={"id": ' + req.params.id + '}&' + apiKeyMLab, JSON.parse(cambio),
    function(err, respuestaMLab, bodypu){
      bodypu.n >0 ? res.send({"msg":"Usuario actualizado"}):res.send(204);
    });
});
//Metodo para eliminar objetos user
app.delete(urlBase + 'users/:id',
 function (req, res) {
   console.log("DELETE" + urlBase +" /colapi/v1");
   var idEliminar = req.params.id;
   var httpClient = requestJson.createClient(urlBaMongo);
   httpClient.put('user?q={"id": ' + req.params.id + '}&' + apiKeyMLab, [],
    function(err, respuestaMLab, bodypu){
      bodypu.removed >0 ? res.send({"msg":"Usuario Eliminado"}):res.send(204);
    });

});
