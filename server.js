var express = require('express');
var bodyParser = require('body-parser');
var app = express();
var port = process.env.port || 3000;
var usersFile = require('./users.json');
var urlBase = "/colapi/v1/";

app.use(bodyParser.json());
//peticion Get con Parametros
app.get(urlBase + 'users/:id/:otro',
 function (req, res) {
   var pos = req.params.id;

   res.send(
    usersFile[pos-1]
    );
  console.log(urlBase + "users/:id");
});
//peticion Get con Query
app.get(urlBase + 'users',
 function (peticion, respuesta) {
   console.log(urlBase + "users/:id Peticion con Query");
   console.log(peticion.query);
   respuesta.send(
    {"msg" : "Get Con Query string"}
    );

});
//Metodo para crear un objeto user.
app.post(urlBase + "users",
 function (req, res) {
   console.log("Peticion Post: "+ urlBase + "users");
   console.log(req.body);
   //res.sendFile("users.json",{root:__dirname});
   var newID = usersFile.length + 1 ;
   var jsonID = {};
   jsonID=req.body;
   jsonID.id = newID;
   usersFile.push(jsonID);
   res.send({"MSG" : "Usuario nuevo  creado ok.", jsonID} );
});
//Metodo Put para modificar el recurso user
app.put(urlBase + 'users/:id',
 function (req, res) {
   console.log("PUT /colapi/v1/users");
   var idBuscar = req.params.id;
   var updateUser = req.body;
   var existe =0;
   console.log(updateUser);
   for (var user in usersFile){
     if(usersFile[user].id==idBuscar){
       console.log("encontrado " + user);
       existe =1;
     }
   }
   if (existe == 0){
     console.log("usuario con id " + idBuscar + "no existe");
   }
   res.send({
    "message" : "Put a Colapi en formato Json" ,
    "version" : "V1"
    });

});
//Metodo para eliminar objetos user
app.delete(urlBase + 'users/:id',
 function (req, res) {
   console.log("DELETE" + urlBase +" /colapi/v1");
   var idEliminar = req.params.id;
   for (var user in usersFile){
     if(usersFile[user].id==idEliminar){
       usersFile.splice(user,1);
       res.send({
        "message" : "Usuario eliminado " ,
        "id" : idEliminar
        });
     }
   }
   res.send({
    "message" : "Usuario a eliminar no Existe"
    });
});
//Servidor escuchando por el puerto 3000
app.listen(port);
console.log("Colapi Escuchando puerto :" + port +"...");
